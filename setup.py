from setuptools import setup, find_packages

setup(
    name='dacp',
    version='0.1',
    packages=find_packages(),
    install_requires=[
        'scipy', 'numpy']
)
